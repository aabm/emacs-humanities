;;; ------------------------------------------------------------------
;; LICENSE
;; Copyright © 2020 Aabm <aabm@disroot.org>
;; Author: Aabm <aabm@disroot.org>
;; URL: <https://git.snopyta.org/aabm/emacs-humanities/>

;; This file is not part of GNU Emacs.
;;
;; This program is free software: you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program, see the file LICENSE. If not, see
;; <http://www.gnu.org/licenses/>
;;; ------------------------------------------------------------------
;; Uma tradução não oficial para português brasileiro da licensa
;; acima pode ser encontrada em:
;; <http://licencas.softwarelivre.org/gpl-3.0.pt-br.html>
;;; ------------------------------------------------------------------

;;; Este é o arquivo de init do Emacs Humanities.
;; Este é o segundo arquivo a ser carregado pelo Emacs usando esta configuração
;; durante a inicialização, logo após o early-init.el. O código aqui tem o
;; objetivo principal de configurar o gerenciador de pacotes e garantir que
;; todo o código em Lisp seja compilado para bytecode. Mais informações sobre
;; toda esta configuração no arquivo config.org.

;;; Configurações iniciais do gerenciador de pacotes do Emacs, o package.el
;; Aqui carregamos o package.el, depois configuramos os repositórios a
;; serem usados por ele, e finalmente o inicializamos.
(require 'package)
(setq package-archives '(("gnu"   . "http://elpa.gnu.org/packages/")
						 ("melpa" . "https://melpa.org/packages/")
						 ("org"   . "https://orgmode.org/elpa/")))
(package-initialize)

;;; Instalando o use-package durante a inicialização.
;; O use-package é um macro para gerenciamento declarativo de pacotes.
;; Usamos ele para automaticamente instalar e configurar a maioria dos
;; pacotes aqui utilizados.
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(setq use-package-always-defer t)

;;; Auto compilação de Elisp
;; Este pacote garante que todo o código em Elisp seja compilado para
;; bytecode antes de ser carregado. Isso faz com que pacotes instalados
;; e arquivos de configuração do usuário sejam carregados mais rapidamente
;; em futuras inicializações.
(setq load-prefer-newer t)
(use-package auto-compile
  :ensure t
  :config
  (auto-compile-on-save-mode)
  (auto-compile-on-load-mode))

;;; Finalmente, carregamos o arquivo config.org, onde toda a magia acontece.
(when (file-readable-p (concat user-emacs-directory "config.org"))
  (org-babel-load-file (concat user-emacs-directory "config.org")))

;; Inicializa o servidor do Emacs, para uso com links do
;; emacsclient. Necessário para uso com o org-roam-server.
(server-start)

;; Após a primeira inicialização, o Emacs deve adicionar um bloco de
;; código abaixo. Não é recomendado editar o bloco manualmente.
