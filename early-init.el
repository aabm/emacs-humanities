;;; ------------------------------------------------------------------
;; LICENSE
;; Copyright © 2020 Aabm <aabm@disroot.org>
;; Author: Aabm <aabm@disroot.org>
;; URL: <https://git.snopyta.org/aabm/emacs-humanities/>

;; This file is not part of GNU Emacs.
;;
;; This program is free software: you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program, see the file LICENSE. If not, see
;; <http://www.gnu.org/licenses/>
;;; ------------------------------------------------------------------
;; Uma tradução não oficial para português brasileiro da licensa
;; acima pode ser encontrada em:
;; <http://licencas.softwarelivre.org/gpl-3.0.pt-br.html>
;;; ------------------------------------------------------------------

;;; Este é o arquivo de early-init do Emacs Humanities.
;; Este é o primeiro arquivo a ser carregado pelo Emacs usando esta
;; configuração durante a inicialização. O código aqui tem o propósito
;; de desabilitar recursos não utilizados por esta configuração, com o
;; intuito de alcançar um tempo de inicialização menor. Após este
;; arquivo, o Emacs carrega o arquivo init.el. Mais informações sobre
;; toda esta configuração no arquivo config.org.

;;; Opções de garbage collection para inicialização mais rápida
;; Este código desabilita toda a garbage collection antes da
;; inicialização aumentando consideravelmente o gasto de memória
;; necessário para o seu início e então cria hooks que reabilitam a GC
;; após a inicialização do Emacs, para evitar problemas de
;; estabilidade e desempenho.
(setq gc-cons-threshold 402653184
		gc-cons-percentage 0.6)
 
(defvar startup/file-name-handler-alist file-name-handler-alist)
(setq file-name-handler-alist nil)

(defun startup/revert-file-name-handler-alist ()
  (setq file-name-handler-alist startup/file-name-handler-alist))
 
(defun startup/reset-gc ()
  (setq gc-cons-threshold 16777216
		gc-cons-percentage 0.1))
 
(add-hook 'emacs-startup-hook 'startup/revert-file-name-handler-alist)
(add-hook 'emacs-startup-hook 'startup/reset-gc)

;;; Evitar carregar o gerenciador de pacotes automaticamente.
;; No Emacs pós versão 27, o gerenciador de pacotes package.el
;; é carregado automaticamente durante a inicialização. Aqui evitamos
;; que isso ocorra, e ainda o impedimos de de automaticamente inserir
;; um certo bloco de código no arquivo de init.el, o bloco das
;; custom-set-variables.
(setq package-enable-at-startup nil
      package--init-file-ensured t)

;;; Desabilitando recursos gráficos que não serão usados depois
;; Aqui impedimos as barras de ferramentas e scroll de serem
;; carregadas. A barra de menu permanece habilitada, pois pode ser
;; útil para iniciantes. Se quiser desabilitá-la, basta remover
;; os ;; do início da linha abaixo e reiniciar o Emacs.

(push '(menu-bar-lines . 0) default-frame-alist)
(push '(tool-bar-lines . 0) default-frame-alist)
(push '(vertical-scroll-bars) default-frame-alist)
(set-window-scroll-bars (minibuffer-window) nil nil)
(setq frame-inhibit-implied-resize t)
